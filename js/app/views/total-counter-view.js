define([
  'jquery', 'underscore', 'backbone'
], function ($, _, Backbone) {

  return Backbone.View.extend({
    el: $('#total'),
    hours: 0,

    initialize: function () {
        var _this = this;
        this.calculate();

    },

    render: function () {
      var _this = this;
      $(this.el).html(this.hours);
    },

    calculate: function () {
      var _this = this;
      var entries = $('#results').find('tr');

      $(entries).each(function (index, h) {
        _this.hours = _this.hours + parseFloat($(h).data('hours'));
        _this.render();
      });

    }

  });

});
