define([
  'jquery', 'underscore', 'backbone',
  'app/views/item-view',
  'app/views/total-counter-view',
  'app/entry-collection',
  'text!app/tpl/list.jst'
], function ($, _, Backbone, ItemView, TotalView, Collection, tpl) {

  return Backbone.View.extend({
    el: $('#entryList'),
    template: _.template(tpl),
    counter: 0,
    totalView: TotalView,
    collection: Collection,

    initialize: function () {
        var _this = this;
        this.collection = new Collection();
        this.fetchCollection();

        this.listenTo(this.collection, 'add', this.render());
    },

    fetchCollection: function () {
        var _this = this;
        this.collection.fetch({success: function () {
            _this.render();
        }});
    },

    render: function () {
      var _this = this;
      $(this.el).html(this.template);

      this.collection.each(function (item) {
          _this.renderItem(item);
      });

    },

    renderItem: function (item) {
      this.counter++;
      var itemView = new ItemView({el: $('#results'), model: item, counter: this.counter});
      itemView.render();

      if (this.counter == this.collection.length) this.totalView = new TotalView();
    }

  });

});

