define(['jquery', 'backbone', 'app/entry-model', 'jqeryuiTimepicker'], function($, Backbone, EntryModel) {
    return Backbone.View.extend({
        el: $('#entryForm'),
        model: new EntryModel(),
        dateInput: $('#startDate'),
        sTimeInput: $('#startTime'),
        eTimeInput: $('#endTime'),
        hoursInput: $('#hours'),
        initialize: function() {
            this.dateInput.datepicker();
            this.sTimeInput.timepicker();
            this.eTimeInput.timepicker();

            $(this.el).on('change', $.proxy(function() {
                this.calculateHours();
            }, this));

            $(this.hoursInput).on('click', function() {
                this.checkForm();
            });

        },
        calculateHours: function() {
            var diff = this.diff(this.sTimeInput.val(), this.eTimeInput.val());
            this.hoursInput.val(diff);
        },
        diff: function(start, end) {
            start = start.split(":");
            end = end.split(":");
            var startDate = new Date(0, 0, 0, start[0], start[1], 0);
            var endDate = new Date(0, 0, 0, end[0], end[1], 0);
            var diff = endDate.getTime() - startDate.getTime();
            var hours = Math.floor(diff / 1000 / 60 / 60);
            diff -= hours * 1000 * 60 * 60;
            var minutes = Math.floor(diff / 1000 / 60);

            // If using time pickers with 24 hours format, add the below line get exact hours
            if (hours < 0) {
                hours = hours + 24;
            }
            var dec_hours = parseInt(hours, 10);
            var dec_minutes = parseInt(minutes, 10);

            return (dec_hours + dec_minutes / 60).toFixed(2);

        },
        checkForm: function() {

            if (this.dateInput.val() !== '' && this.sTimeInput.val() !== '' && this.eTimeInput.val() !== '' && this.hoursInput.val() !== '') {
                this.model.set({
                    id:    this.dateInput.val(),
                    date:  this.dateInput.val(),
                    sTime: this.sTimeInput.val(),
                    eTime: this.eTimeInput.val(),
                    hours: this.hoursInput.val()
                });
                this.model.save();
                window.router.navigate('reload');
            } else {
                $(this.el).dialog();
            }
        }

    });
});

