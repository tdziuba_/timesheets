define(['jquery', 'underscore', 'backbone', 'app/router', 'jqueryui', 'foundation'], function ($, _, Backbone, Router) {
  $(document).foundation();

  window.router = new Router();
  Backbone.history.start();
})
