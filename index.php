<!doctype html>
<html>
  <head>
    <title>Timesheets</title>
    <meta charset="utf-8">
    <link href="css/app.css" rel="stylesheet">
  </head>
  <body>
    
    <?php include('partials/header.phtml'); ?>
    <?php include('partials/main.phtml'); ?>
    <?php include('partials/footer.phtml'); ?>
    
    <script src="js/libs/require.js/require.js" data-main="js/config"></script>
  </body>
</html>

