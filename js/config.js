requirejs.config({
  baseUrl: 'js/libs',
  paths: {
    app: '../app/',
    jquery: 'jquery/jquery',
    jqueryui: 'jqueryui/jquery-ui',
    jqeryuiTimepicker: 'jqueryui/jquery-ui-timepicker-addon',
    foundation: '../../bower_components/foundation/js/foundation.min',
    backbone: 'backbone.js/backbone',
    underscore: 'underscore.js/underscore',
    text: 'require-text/text'
  },
  shim: {
    underscore: {
      exports: '_'
    },
    backbone: {
      deps: ['jquery'],
      exports: 'Backbone'
    },
    jqueryui: {
      deps: ['jquery'],
      exports: '$'
    },
    jqeryuiTimepicker: {
      deps: ['jquery', 'jqueryui'],
      exports: '$'
    },
    foundation: {
      deps: ['jquery']
    }
  }
});

require(['../main']);
