define(['jquery', 'backbone'], function($, Backbone) {
    return Backbone.Model.extend({
        url: 'rest.php',
        //idAttribute: "date",
        defaults: {
            id: null,
            date: null,
            sTime: null,
            eTime: null,
            hours: null
        }
    });
});



