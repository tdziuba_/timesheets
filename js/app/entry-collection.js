define(['jquery', 'backbone', 'app/entry-model'], function ($, Backbone, EntryModel) {
    return Backbone.Collection.extend({
        url: 'hours.json',
        model: EntryModel
    });
});



