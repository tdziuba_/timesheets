define(['jquery', 'underscore', 'backbone', 'text!app/tpl/item.jst'], function ($, _, Backbone, tpl) {
  return Backbone.View.extend({
    el: $('#results'),
    model: null,
    counter: 0,
    tpl: _.template(tpl),
    render: function () {
      $(this.el).append( this.tpl({ item: this.model.toJSON(), counter: this.options.counter }) );
    }
  });
});

