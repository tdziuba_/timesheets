define(['jquery', 'backbone', 'app/views/form-view', 'app/views/list-view'], function ($, Backbone, FormView, ListView) {
  return Backbone.Router.extend({
    routes: {
      "edit/:id": "edit",
      "save": "save",
      "update/:id": "update",
      "reload": "reload",
      "*actions": "default"
    },

    initialize: function () {
      this.oformView = new FormView();
      this.olistView = new ListView();
    },

    edit: function (id) {
      console.log('edit: '+id);
    },

    update: function (id) {
      console.log('update: '+id);
    },

    save: function () {
      this.oformView.checkForm();
    },

    reload: function () {
        this.olistView.fetchCollection();
    },

  default: function () {
      //this.olistView.initialize();
  }

  });
});


