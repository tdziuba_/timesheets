<?php

class Rest {
  protected static $file = 'hours.json';

  public static function getData()
  {
    return json_decode( file_get_contents(self::$file) );
  }

  public static function store() 
  {
    $data = self::getData();
    $json = file_get_contents('php://input');
    $d = json_decode($json);

    if ($d->id == null) {
        $d->id = $d->date;
    }
    $data[] = $d;
    self::makeBackup();

    file_put_contents(self::$file, json_encode($data));
  }

  public static function update($data)
  {
    $d = self::getData();
//    if (isset($d[$data['id']])) {
//      $d[$data['id']] = $data;
//    }
    self::makeBackup();

    file_put_contents(dirname(__FILE__).DIRECTORY_SEPARATOR.self::$file, json_encode($d));
  }

  protected static function makeBackup()
  {
    $data = self::getData();
    var_dump($data);
    $dir = dirname(__FILE__);
    $ds = DIRECTORY_SEPARATOR;
    file_put_contents($dir.$ds.'backup'.$ds.self::$file.'_backup_'.date('Y_m_d_h_i_s'), json_encode($data));
  }
}

if($_SERVER['REQUEST_METHOD'] == 'GET') {
    header('Accept: application/json');
    echo json_encode(Rest::getData());
}
if($_SERVER['REQUEST_METHOD'] == 'POST' || $_SERVER['REQUEST_METHOD'] == 'PUT') {
    echo Rest::store();
}